#!/usr/bin/env bash

# Load config from file
source <(grep=config_apvm.ini)

# This will download the relevant configuration files from the S3 bucket and make the config file
if [[ ${GET_CONFIG} == true ]]
then
    IFS="," read -r -a SERVICE_ARRAY <<< "$SERVICES_LIST"
    for SERVICE in "${SERVICE_ARRAY[@]}"
    do
        aws s3 cp "s3://tac-config-network/${CONFIG_ENV}/config_${SERVICE}.yml" .
    done
    python make_config.py "${SERVICES_LIST}" "${DEPLOYMENT_MODE}"
fi

if [[ ${PUSH_CONFIG} == true ]]
then
    export SKIP_TAGS=none
else
    export SKIP_TAGS=config
fi

cat << EOF > ssh-script.sh
echo "__\///\ # Add IP: ${IP_ADDRESS}  to known hosts"
ssh-keyscan -p ${SSH_PORT} -H ${IP_ADDRESS} >> ~/.ssh/known_hosts
echo "__\///\ # ssh-add ${SSH_KEY}"
ssh-add ${SSH_KEY}
echo "__\///\ # ssh into ${IP_ADDRESS}"
ssh ${USER_NAME}@${IP_ADDRESS} -p ${SSH_PORT} "sudo apt-get update && echo 'Y' | sudo apt-get install python-pip && pip install ansible==2.2"
echo "__\///\ # Run ansible-playbook on ${IP_ADDRESS} "
export ANSIBLE_STDOUT_CALLBACK=debug
echo "[default]host_key_checking = False" > ansible.cfg
ansible-playbook -i "${IP_ADDRESS}," -u ${USER_NAME} playbook_make_crypto_key_pair.yml -v --extra-vars "user_name=${USER_NAME}" -e ansible_ssh_port=${SSH_PORT}
EOF
ssh-agent bash ssh-script.sh
rm -rf ssh-script.sh
