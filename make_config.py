#!/home/sbharadwaj/.conda/envs/via-science/bin/python

import yaml
import sys
import os

if __name__ == "__main__":
    component_config = dict()
    service_list = list()

    try:
        service_list = sys.argv[1].split(",")
        # deployment_mode = sys.argv[2]
    except IndexError:
        print("ERROR: Expecting list of services as parameter...\n"
              "Example name of service config file should be: 'config_<name_of_service>.yml'")
        sys.exit(1)
    print("List of services for this component: {}".format(service_list))
    for service in service_list:
        service_config_file = "config_{}.yml".format(service)
        if os.path.exists(service_config_file):
            print("Reading File {} ...".format(service_config_file))
            with open(service_config_file) as r:
                service_config = yaml.load(r)
                component_config.update(service_config)
        else:
            print("ERROR: File {} does not exist!!!".format(service_config_file))
            sys.exit(1)
    print("Writing Final Component Config...")
    with open("config_network.yml", "w") as w:
        w.write(yaml.dump(component_config, default_flow_style=False))
