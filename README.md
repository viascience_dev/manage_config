# Manage Network Configuration on Deployed VMs
###### Prerequisite: Access to the right AWS S3 bucket from the terminal being used and ansible>=2.2 need to be installed
#### Edit configuration in `config_vm.ini`

1. Set key path in `SSH_KEY` to the required local `pem` file.

2. Set the `IP_ADDRESS` of VM. 
[TODO: use inventory to manage multiple VMs]

3. Set the `SSH_PORT` of VM.
Set it to `2222` if this a Vagrant VM else set it to `22`

4. Set the `USER_NAME` of VM to login. 
Set it to `vagrant` if this a Vagrant VM else set it to `ubuntu` if on AWS

5. Set `PUSH_CONFIG` to `true` if you need to change configuration, else, set to `false`. 
When `true`, the script will expect the network configuration file `config_network.yml` in the root of this directory. 

6. Set the name of the playbook in `PLAYBOOK` to be run by this script

7. Set the `DEPLOYMENT_MODE` to either `dev` or `staging` or `production`

8. Set `USER_ROLE` if required `AP` for APVM and `DO` for DOVM

9. Set `RUN_TEST` mode to `all` or `unittest`

10. Set list of Services in `SERVICES_LIST`. 
This needs to match with file name on S3 bucket of Network Configuration. 
Example: `SERVICES_LIST=nodejs,scripts,tacnode,usermanagement`

11. Set Network Configuration Environment in `CONFIG_ENV`. 
Example: `CONFIG_ENV=vagrant`

This script will download the relevant configuration files from the S3 bucket and make the config file

Run the script run_restart_apvm with the command: `./run_restart_apvm.sh`

this will in turn run the script `ssh-script.sh` with the above options and is cleaned up at the end of execution
